package com.lpl.businessrules.extender;

import java.util.List;

public class Extender {

	public static boolean isDivisibleByFive(double allowedIncrements) {
		return ((allowedIncrements % 5) == 0);
		/*
		 * boolean flag = false; double reminder = allowedIncrements % 5;
		 * if(reminder == 0){ flag = true; }else { flag = false; } return flag;
		 */
	}

	public static boolean isCountryBlocked(String country,
			List<String> blockedCountryList) {
		boolean flag = false;
		if (blockedCountryList != null && country != null) {
			/*
			 * for (String blockedCountry : blockedCountryList) {
			 * if(blockedCountry.equalsIgnoreCase(country)){ flag = true; break;
			 * } }
			 */
			if (blockedCountryList.contains(country))
				flag = true;

		}
		return flag;
	}
}
