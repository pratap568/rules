package com.lpl.businessrules.domain;


public class DOCADocuments {
	
	private String formCode;
	private String formStatus;
	private String formDescription;
	private String formName;
	
	private String documentCode;
	private String documentDescription;
	private String documentStatusCode;
	private String supplementForm;
	
	public DOCADocuments() {
		formCode = "";
		formStatus = "";
		formDescription = "";
		formName ="";
		
		documentCode = "";
		documentDescription = "";
		documentStatusCode = "";
		supplementForm = "";
	}

	public String getFormCode() {
		return formCode;
	}

	public void setFormCode(String formCode) {
		this.formCode = formCode;
	}

	public String getFormStatus() {
		return formStatus;
	}

	public void setFormStatus(String formStatus) {
		this.formStatus = formStatus;
	}

	public String getFormDescription() {
		return formDescription;
	}

	public void setFormDescription(String formDescription) {
		this.formDescription = formDescription;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public String getDocumentCode() {
		return documentCode;
	}

	public void setDocumentCode(String documentCode) {
		this.documentCode = documentCode;
	}

	public String getDocumentDescription() {
		return documentDescription;
	}

	public void setDocumentDescription(String documentDescription) {
		this.documentDescription = documentDescription;
	}

	public String getDocumentStatusCode() {
		return documentStatusCode;
	}

	public void setDocumentStatusCode(String documentStatusCode) {
		this.documentStatusCode = documentStatusCode;
	}

	public String getSupplementForm() {
		return supplementForm;
	}

	public void setSupplementForm(String supplementForm) {
		this.supplementForm = supplementForm;
	}	

	
	
}
