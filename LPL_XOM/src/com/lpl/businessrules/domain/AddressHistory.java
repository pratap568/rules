package com.lpl.businessrules.domain;

import java.util.Date;

public class AddressHistory {
	
	private Date changedDate;
	private String addressChangeCode;
	private String addressType;
	private String country;
	private String state;
	private String houseNo;
	private String city;
	private String street;
	private double streetNumber;
	
	public AddressHistory()
	{
		addressChangeCode="";
		addressType="";
		country="";
		state="";
		houseNo="";
		city="";
		street="";
		streetNumber=0.0d;
		
	}

	
	
	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Date getChangedDate() {
		return changedDate;
	}

	public void setChangedDate(Date changedDate) {
		this.changedDate = changedDate;
	}

	public String getAddressChangeCode() {
		return addressChangeCode;
	}

	public void setAddressChangeCode(String addressChangeCode) {
		this.addressChangeCode = addressChangeCode;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public double getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(double streetNumber) {
		this.streetNumber = streetNumber;
	}
	
}
