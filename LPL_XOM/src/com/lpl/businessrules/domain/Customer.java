package com.lpl.businessrules.domain;


import java.util.Date;

public class Customer {
	
	private String customerId;
	private String name;
	private Date dateOfBirth;
	private AddressHistory mailingAddress;
	private AddressHistory residenceAddress;
	private String relationshipToAccount;
	//
	//private List<InternalAccount> internalAccounts;
	//
	private Date dateOfDeath;
	
	public Customer() {
		
		customerId = "";
		name = "";
		dateOfBirth = new Date();
		mailingAddress = new AddressHistory();
		residenceAddress =new AddressHistory();
		relationshipToAccount = "";
		
		//internalAccounts = new ArrayList<InternalAccount>();
		
		dateOfDeath = new Date();

	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public AddressHistory getMailingAddress() {
		if(mailingAddress == null)
			mailingAddress = new AddressHistory();
		return mailingAddress;
	}

	public void setMailingAddress(AddressHistory mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	public AddressHistory getResidenceAddress() {
		if(residenceAddress == null)
			residenceAddress = new AddressHistory();
		return residenceAddress;
	}

	public void setResidenceAddress(AddressHistory residenceAddress) {
		this.residenceAddress = residenceAddress;
	}

	public String getRelationshipToAccount() {
		return relationshipToAccount;
	}

	public void setRelationshipToAccount(String relationshipToAccount) {
		this.relationshipToAccount = relationshipToAccount;
	}

	public Date getDateOfDeath() {
		return dateOfDeath;
	}

	public void setDateOfDeath(Date dateOfDeath) {
		this.dateOfDeath = dateOfDeath;
	}
	
	
}
