package com.lpl.businessrules.domain;


import java.util.ArrayList;
import java.util.List;


public class Advisor {

	private String advisorId;
	private String statusCode;
	private String employmentStatus;
	private String branchId;
	// private String employeeClass;
	// private String employeeClassCode;
	//
	private List<AdvisorAddress> advisorAddresses;
	//
	private String advisoryAgreementStatus;

	public Advisor() {
		advisorId = "";
		statusCode = "";
		branchId = "";
		// employeeClass = "";
		// employeeClassCode = "";

		advisorAddresses = new ArrayList<AdvisorAddress>();

	}

	public String getAdvisoryAgreementStatus() {
		return advisoryAgreementStatus;
	}

	public void setAdvisoryAgreementStatus(String advisoryAgreementStatus) {
		this.advisoryAgreementStatus = advisoryAgreementStatus;
	}

	public void setEmploymentStatus(String employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public String getAdvisorId() {
		return advisorId;
	}

	public void setAdvisorId(String advisorId) {
		this.advisorId = advisorId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getEmploymentStatus() {
		return employmentStatus;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	/*
	 * public String getEmployeeClass() { return employeeClass; }
	 * 
	 * 
	 * public void setEmployeeClass(String employeeClass) { this.employeeClass =
	 * employeeClass; }
	 * 
	 * 
	 * public String getEmployeeClassCode() { return employeeClassCode; }
	 * 
	 * 
	 * public void setEmployeeClassCode(String employeeClassCode) {
	 * this.employeeClassCode = employeeClassCode; }
	 */

	public List<AdvisorAddress> getAdvisorAddresses() {
		if (advisorAddresses == null)
			advisorAddresses = new ArrayList<AdvisorAddress>();
		return advisorAddresses;
	}

	public void setAdvisorAddresses(List<AdvisorAddress> advisorAddresses) {
		this.advisorAddresses = advisorAddresses;
	}

}
