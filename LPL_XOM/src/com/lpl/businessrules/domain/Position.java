package com.lpl.businessrules.domain;

public class Position {
	
	private String securityId;
	private String positionLocationCode;
	private long heldShareAmount;
	private String primaryMarginabilityCode;
	private String secondaryMarginabilityCode;
	private double marketPrice;
	private double shareValue;
	private Security security;
	
	public Position() {
		securityId ="";
		positionLocationCode = "";
		heldShareAmount = 0L;
		primaryMarginabilityCode = "";
		secondaryMarginabilityCode ="";
		marketPrice = 0.0d;
		shareValue = 0.0d;
		security = new Security();
		
	}

	public String getSecurityId() {
		return securityId;
	}

	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}

	public String getPositionLocationCode() {
		return positionLocationCode;
	}

	public void setPositionLocationCode(String positionLocationCode) {
		this.positionLocationCode = positionLocationCode;
	}

	public long getHeldShareAmount() {
		return heldShareAmount;
	}

	public void setHeldShareAmount(long heldShareAmount) {
		this.heldShareAmount = heldShareAmount;
	}

	public String getPrimaryMarginabilityCode() {
		return primaryMarginabilityCode;
	}

	public void setPrimaryMarginabilityCode(String primaryMarginabilityCode) {
		this.primaryMarginabilityCode = primaryMarginabilityCode;
	}

	public String getSecondaryMarginabilityCode() {
		return secondaryMarginabilityCode;
	}

	public void setSecondaryMarginabilityCode(String secondaryMarginabilityCode) {
		this.secondaryMarginabilityCode = secondaryMarginabilityCode;
	}

	public double getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public double getShareValue() {
		return shareValue;
	}

	public void setShareValue(double shareValue) {
		this.shareValue = shareValue;
	}

	public Security getSecurity() {
		if(security == null)
			security = new Security();
		return security;
	}

	public void setSecurity(Security security) {
		this.security = security;
	}	
	
	
	
}
