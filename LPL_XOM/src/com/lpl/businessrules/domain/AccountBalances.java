package com.lpl.businessrules.domain;

public class AccountBalances {

	private String accontId;
	private double accruedMarginInterest;
	private double accruedCreditInterest;
	private double cashBalance;
	private double marginBalance;
	private double marginCashAvailable;
	private double marginEquityPercent;
	private double marginShortPositions;
	private double moneyMarketBalanceA;
	private double moneyMarketBalanceB;
	private double totalAccountValue;
	private double settledCashBalance;
	private double unsettledFunds;
	private double unsettledTrades;
	private double accruedMMDividend;
	private double closingCosts;
	private double fedMarginCall;
	private double houseMarginCall;
	private double MMABalanceSellFirstBalance;
	private String MMABalanceSellFirstCode;
	private double MMABalanceSellSecondBalance;
	private String MMABalanceSellSecondCode;
	private double mutualFundsPendingBuys;
	private double mutualFundsPendingSells;
	private double PTDAmount;
	private double recentDeposits;
	private double variableAnnuitiesFunds;
	private double currentBalance;
	private double annuitiesDeductions;
	private double interestDeductions;
	private double pendingTransfers;
	private double marginInterests;
	private double annualFee;
	private double loanAccountBalance;
	private double marketPrice = 0.0d;
	private double settledShareQuantity = 0.0d;

	public AccountBalances() {
		accontId = "";
		accruedMarginInterest = 0.0d;
		accruedCreditInterest = 0.0d;
		cashBalance = 0.0d;
		marginBalance = 0.0d;
		marginCashAvailable = 0.0d;
		marginEquityPercent = 0.0d;
		marginShortPositions = 0.0d;
		moneyMarketBalanceA = 0.0d;
		moneyMarketBalanceB = 0.0d;
		totalAccountValue = 0.0d;
		settledCashBalance = 0.0d;
		unsettledFunds = 0.0d;
		unsettledTrades = 0.0d;
		accruedMMDividend = 0.0d;
		closingCosts = 0.0d;
		fedMarginCall = 0.0d;
		houseMarginCall = 0.0d;
		MMABalanceSellFirstBalance = 0.0d;
		MMABalanceSellFirstCode = "";
		MMABalanceSellSecondBalance = 0.0d;
		MMABalanceSellSecondCode = "";
		mutualFundsPendingBuys = 0.0d;
		mutualFundsPendingSells = 0.0d;
		PTDAmount = 0.0d;
		recentDeposits = 0.0d;
		variableAnnuitiesFunds = 0.0d;
		currentBalance = 0.0d;
		annuitiesDeductions = 0.0d;
		interestDeductions = 0.0d;
		pendingTransfers = 0.0d;
		marginInterests = 0.0d;
		annualFee = 0.0d;
		loanAccountBalance = 0.0d;

	}

	public String getAccontId() {
		return accontId;
	}

	public void setAccontId(String accontId) {
		this.accontId = accontId;
	}

	public double getAccruedMarginInterest() {
		return accruedMarginInterest;
	}

	public void setAccruedMarginInterest(double accruedMarginInterest) {
		this.accruedMarginInterest = accruedMarginInterest;
	}

	public double getAccruedCreditInterest() {
		return accruedCreditInterest;
	}

	public void setAccruedCreditInterest(double accruedCreditInterest) {
		this.accruedCreditInterest = accruedCreditInterest;
	}

	public double getCashBalance() {
		return cashBalance;
	}

	public void setCashBalance(double cashBalance) {
		this.cashBalance = cashBalance;
	}

	public double getMarginBalance() {
		return marginBalance;
	}

	public void setMarginBalance(double marginBalance) {
		this.marginBalance = marginBalance;
	}

	public double getMarginCashAvailable() {
		return marginCashAvailable;
	}

	public void setMarginCashAvailable(double marginCashAvailable) {
		this.marginCashAvailable = marginCashAvailable;
	}

	public double getMarginEquityPercent() {
		return marginEquityPercent;
	}

	public void setMarginEquityPercent(double marginEquityPercent) {
		this.marginEquityPercent = marginEquityPercent;
	}

	public double getMarginShortPositions() {
		return marginShortPositions;
	}

	public void setMarginShortPositions(double marginShortPositions) {
		this.marginShortPositions = marginShortPositions;
	}

	public double getMoneyMarketBalanceA() {
		return moneyMarketBalanceA;
	}

	public void setMoneyMarketBalanceA(double moneyMarketBalanceA) {
		this.moneyMarketBalanceA = moneyMarketBalanceA;
	}

	public double getMoneyMarketBalanceB() {
		return moneyMarketBalanceB;
	}

	public void setMoneyMarketBalanceB(double moneyMarketBalanceB) {
		this.moneyMarketBalanceB = moneyMarketBalanceB;
	}

	public double getTotalAccountValue() {
		return totalAccountValue;
	}

	public void setTotalAccountValue(double totalAccountValue) {
		this.totalAccountValue = totalAccountValue;
	}

	public double getSettledCashBalance() {
		return settledCashBalance;
	}

	public void setSettledCashBalance(double settledCashBalance) {
		this.settledCashBalance = settledCashBalance;
	}

	public double getUnsettledFunds() {
		return unsettledFunds;
	}

	public void setUnsettledFunds(double unsettledFunds) {
		this.unsettledFunds = unsettledFunds;
	}

	public double getUnsettledTrades() {
		return unsettledTrades;
	}

	public void setUnsettledTrades(double unsettledTrades) {
		this.unsettledTrades = unsettledTrades;
	}

	public double getAccruedMMDividend() {
		return accruedMMDividend;
	}

	public void setAccruedMMDividend(double accruedMMDividend) {
		this.accruedMMDividend = accruedMMDividend;
	}

	public double getClosingCosts() {
		return closingCosts;
	}

	public void setClosingCosts(double closingCosts) {
		this.closingCosts = closingCosts;
	}

	public double getFedMarginCall() {
		return fedMarginCall;
	}

	public void setFedMarginCall(double fedMarginCall) {
		this.fedMarginCall = fedMarginCall;
	}

	public double getHouseMarginCall() {
		return houseMarginCall;
	}

	public void setHouseMarginCall(double houseMarginCall) {
		this.houseMarginCall = houseMarginCall;
	}

	public double getMMABalanceSellFirstBalance() {
		return MMABalanceSellFirstBalance;
	}

	public void setMMABalanceSellFirstBalance(double mMABalanceSellFirstBalance) {
		MMABalanceSellFirstBalance = mMABalanceSellFirstBalance;
	}

	public String getMMABalanceSellFirstCode() {
		return MMABalanceSellFirstCode;
	}

	public void setMMABalanceSellFirstCode(String mMABalanceSellFirstCode) {
		MMABalanceSellFirstCode = mMABalanceSellFirstCode;
	}

	public double getMMABalanceSellSecondBalance() {
		return MMABalanceSellSecondBalance;
	}

	public void setMMABalanceSellSecondBalance(
			double mMABalanceSellSecondBalance) {
		MMABalanceSellSecondBalance = mMABalanceSellSecondBalance;
	}

	public String getMMABalanceSellSecondCode() {
		return MMABalanceSellSecondCode;
	}

	public void setMMABalanceSellSecondCode(String mMABalanceSellSecondCode) {
		MMABalanceSellSecondCode = mMABalanceSellSecondCode;
	}

	public double getMutualFundsPendingBuys() {
		return mutualFundsPendingBuys;
	}

	public void setMutualFundsPendingBuys(double mutualFundsPendingBuys) {
		this.mutualFundsPendingBuys = mutualFundsPendingBuys;
	}

	public double getMutualFundsPendingSells() {
		return mutualFundsPendingSells;
	}

	public void setMutualFundsPendingSells(double mutualFundsPendingSells) {
		this.mutualFundsPendingSells = mutualFundsPendingSells;
	}

	public double getPTDAmount() {
		return PTDAmount;
	}

	public void setPTDAmount(double pTDAmount) {
		PTDAmount = pTDAmount;
	}

	public double getRecentDeposits() {
		return recentDeposits;
	}

	public void setRecentDeposits(double recentDeposits) {
		this.recentDeposits = recentDeposits;
	}

	public double getVariableAnnuitiesFunds() {
		return variableAnnuitiesFunds;
	}

	public void setVariableAnnuitiesFunds(double variableAnnuitiesFunds) {
		this.variableAnnuitiesFunds = variableAnnuitiesFunds;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public double getAnnuitiesDeductions() {
		return annuitiesDeductions;
	}

	public void setAnnuitiesDeductions(double annuitiesDeductions) {
		this.annuitiesDeductions = annuitiesDeductions;
	}

	public double getInterestDeductions() {
		return interestDeductions;
	}

	public void setInterestDeductions(double interestDeductions) {
		this.interestDeductions = interestDeductions;
	}

	public double getPendingTransfers() {
		return pendingTransfers;
	}

	public void setPendingTransfers(double pendingTransfers) {
		this.pendingTransfers = pendingTransfers;
	}

	public double getMarginInterests() {
		return marginInterests;
	}

	public void setMarginInterests(double marginInterests) {
		this.marginInterests = marginInterests;
	}

	public double getAnnualFee() {
		return annualFee;
	}

	public void setAnnualFee(double annualFee) {
		this.annualFee = annualFee;
	}

	public double getLoanAccountBalance() {
		return loanAccountBalance;
	}

	public void setLoanAccountBalance(double loanAccountBalance) {
		this.loanAccountBalance = loanAccountBalance;
	}

	public double getMarketPrice() {
		return marketPrice;
	}

	public void setMarketPrice(double marketPrice) {
		this.marketPrice = marketPrice;
	}

	public double getSettledShareQuantity() {
		return settledShareQuantity;
	}

	public void setSettledShareQuantity(double settledShareQuantity) {
		this.settledShareQuantity = settledShareQuantity;
	}

}
