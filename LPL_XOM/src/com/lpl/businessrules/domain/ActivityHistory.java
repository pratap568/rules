package com.lpl.businessrules.domain;

import java.util.ArrayList;
import java.util.List;

public class ActivityHistory {
	
	private String activityId;
	private List<TransactionHistory> activityHistory;
	private String previousRollovers;
	
	private Customer customer;
	
	public ActivityHistory() {
		activityId = new String();
		activityHistory = new ArrayList<>();
		previousRollovers = new String();
		
		customer = new Customer();
	}
	public String getActivityId() {
		return activityId;
	}
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}
	
	public List<TransactionHistory> getActivityHistory() {
		if(activityHistory == null)
			activityHistory = new ArrayList<TransactionHistory>();
		return activityHistory;
	}
	public void setActivityHistory(List<TransactionHistory> activityHistory) {
		this.activityHistory = activityHistory;
	}
	public String getPreviousRollovers() {
		return previousRollovers;
	}
	public void setPreviousRollovers(String previousRollovers) {
		this.previousRollovers = previousRollovers;
	}
	
	
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	
	
}
