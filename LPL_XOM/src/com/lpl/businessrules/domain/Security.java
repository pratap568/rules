package com.lpl.businessrules.domain;


public class Security {
	
	private String securityId;
	private String bondRating;
	private String assetClassSecurityType;
	private String optionType;	
	private String bondType;
	private String securityLocationCode;
	
	//private List<Position> positions;
	
	private double minimumPrice;
	private String securityType;
	private int securityQuantity;
	
	
	public Security() {
		securityId = "";
		bondRating = "";
		assetClassSecurityType = "";
		optionType="";
		bondType = "";
		securityLocationCode="";
		
	//	positions = new ArrayList<Position>();
		
		minimumPrice = 0.0d;
		securityType = "";
		securityQuantity=0;
	}


	public String getSecurityId() {
		return securityId;
	}


	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}


	public String getBondRating() {
		return bondRating;
	}


	public void setBondRating(String bondRating) {
		this.bondRating = bondRating;
	}


	public String getAssetClassSecurityType() {
		return assetClassSecurityType;
	}


	public void setAssetClassSecurityType(String assetClassSecurityType) {
		this.assetClassSecurityType = assetClassSecurityType;
	}


	public String getOptionType() {
		return optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public String getBondType() {
		return bondType;
	}


	public void setBondType(String bondType) {
		this.bondType = bondType;
	}


	public String getSecurityLocationCode() {
		return securityLocationCode;
	}


	public void setSecurityLocationCode(String securityLocationCode) {
		this.securityLocationCode = securityLocationCode;
	}


	public double getMinimumPrice() {
		return minimumPrice;
	}


	public void setMinimumPrice(double minimumPrice) {
		this.minimumPrice = minimumPrice;
	}


	public String getSecurityType() {
		return securityType;
	}


	public void setSecurityType(String securityType) {
		this.securityType = securityType;
	}


	public int getSecurityQuantity() {
		return securityQuantity;
	}


	public void setSecurityQuantity(int securityQuantity) {
		this.securityQuantity = securityQuantity;
	}	
	
}
