package com.lpl.businessrules.domain;

public class AdvisorAddress {
	
	private AddressHistory homeAddress;
	private AddressHistory OBAAddress;
	private AddressHistory branchAddress;
	
	public AdvisorAddress() {
		
		homeAddress = new AddressHistory();	
		OBAAddress = new AddressHistory();	
		branchAddress = new AddressHistory();
		
	}
	public AddressHistory getHomeAddress() {
		if(homeAddress == null)
			homeAddress = new AddressHistory();
		return homeAddress;
	}
	
	public void setHomeAddress(AddressHistory homeAddress) {
		this.homeAddress = homeAddress;
	}
	public AddressHistory getOBAAddress() {
		if(OBAAddress == null)
			OBAAddress = new AddressHistory();
		return OBAAddress;
	}
	public void setOBAAddress(AddressHistory oBAAddress) {
		OBAAddress = oBAAddress;
	}
	public AddressHistory getBranchAddress() {
		if(branchAddress == null)
			branchAddress = new AddressHistory();
		return branchAddress;
	}
	public void setBranchAddress(AddressHistory branchAddress) {
		this.branchAddress = branchAddress;
	}	

}
