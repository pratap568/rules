package com.lpl.businessrules.domain;


import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class Request {

	private String requestId;
	private String documentationRequired;
	private Date requestDate;
	private String requestType;
	private String transactionType;
	private String fullPayoutIndicator;
	private String enteredBy;
	private String marginOptIn;
	private double transactionAmount;
	private String transactionShareQty;
	private Date alottedTaxYear;
	private String ownershipOfFunds;
	private String destinationOfFunds;
	private String recipientOfFunds;
	private String transactionReason;
	private String journalType;
	private String stateTaxIndicator;
	private String federalTaxIndicator;
	private double stateTaxAmount;
	private double federalTaxAmount;
	private String stateTaxWithholdingCode;
	private String federalTaxWithholdingCode;
	private double stateTaxPercentage;
	private String checkDeliveryMethod;
	private String checkDeliveryAddress;
	private String checkRecipient;
	private AddressHistory checkRecipientAddress;
	private String useExistingInstructions;
	private String distributionFundsForNonLPLInvestement;
	//
	private List<InternalAccount> internalAccounts;
	private List<Position> positions;
	//
	private Date transactionDate;
	private String marginAgreement;
	private String typeOfFunds;
	private String transactionRequires;
	private String SPADNotes;
	private String repId;
	private int taxYearContributionPeriod;
	private String w4PForm;
	private String selectedNetGrossDistribution;
	private double totalContributionsToDate;
	private double totalContributionReasonContributionsToDate;
	private String journalDepartmentDescription;

	public Request() {
		requestId = "";
		requestDate = new Date();
		fullPayoutIndicator = "";
		enteredBy = "";
		transactionAmount = 0.0d;
		transactionShareQty = "";
		alottedTaxYear = new Date();
		stateTaxIndicator = "";
		federalTaxIndicator = "";
		stateTaxAmount = 0.0d;
		federalTaxAmount = 0.0d;
		stateTaxWithholdingCode = "";
		federalTaxWithholdingCode = "";
		stateTaxPercentage = 0.0d;
		checkRecipient = "";
		useExistingInstructions = "";
		distributionFundsForNonLPLInvestement = "";
		internalAccounts = new ArrayList<InternalAccount>();
		journalDepartmentDescription = "";
		positions = new ArrayList<Position>();
		transactionDate = new Date();
		repId = "";
		taxYearContributionPeriod = 0;
		requestType = "";
		transactionType = "";
		totalContributionsToDate = 0.0d;
		totalContributionReasonContributionsToDate = 0.0d;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getFullPayoutIndicator() {
		return fullPayoutIndicator;
	}

	public void setFullPayoutIndicator(String fullPayoutIndicator) {
		this.fullPayoutIndicator = fullPayoutIndicator;
	}

	public String getEnteredBy() {
		return enteredBy;
	}

	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	public double getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public String getTransactionShareQty() {
		return transactionShareQty;
	}

	public void setTransactionShareQty(String transactionShareQty) {
		this.transactionShareQty = transactionShareQty;
	}

	public Date getAlottedTaxYear() {
		return alottedTaxYear;
	}

	public void setAlottedTaxYear(Date alottedTaxYear) {
		this.alottedTaxYear = alottedTaxYear;
	}

	public String getStateTaxIndicator() {
		return stateTaxIndicator;
	}

	public void setStateTaxIndicator(String stateTaxIndicator) {
		this.stateTaxIndicator = stateTaxIndicator;
	}

	public String getFederalTaxIndicator() {
		return federalTaxIndicator;
	}

	public void setFederalTaxIndicator(String federalTaxIndicator) {
		this.federalTaxIndicator = federalTaxIndicator;
	}

	public double getStateTaxAmount() {
		return stateTaxAmount;
	}

	public void setStateTaxAmount(double stateTaxAmount) {
		this.stateTaxAmount = stateTaxAmount;
	}

	public double getFederalTaxAmount() {
		return federalTaxAmount;
	}

	public void setFederalTaxAmount(double federalTaxAmount) {
		this.federalTaxAmount = federalTaxAmount;
	}

	public String getStateTaxWithholdingCode() {
		return stateTaxWithholdingCode;
	}

	public void setStateTaxWithholdingCode(String stateTaxWithholdingCode) {
		this.stateTaxWithholdingCode = stateTaxWithholdingCode;
	}

	public String getFederalTaxWithholdingCode() {
		return federalTaxWithholdingCode;
	}

	public void setFederalTaxWithholdingCode(String federalTaxWithholdingCode) {
		this.federalTaxWithholdingCode = federalTaxWithholdingCode;
	}

	public double getStateTaxPercentage() {
		return stateTaxPercentage;
	}

	public void setStateTaxPercentage(double stateTaxPercentage) {
		this.stateTaxPercentage = stateTaxPercentage;
	}

	public String getCheckRecipient() {
		return checkRecipient;
	}

	public void setCheckRecipient(String checkRecipient) {
		this.checkRecipient = checkRecipient;
	}

	public String getUseExistingInstructions() {
		return useExistingInstructions;
	}

	public void setUseExistingInstructions(String useExistingInstructions) {
		this.useExistingInstructions = useExistingInstructions;
	}

	public String getDistributionFundsForNonLPLInvestement() {
		return distributionFundsForNonLPLInvestement;
	}

	public void setDistributionFundsForNonLPLInvestement(
			String distributionFundsForNonLPLInvestement) {
		this.distributionFundsForNonLPLInvestement = distributionFundsForNonLPLInvestement;
	}

	public List<InternalAccount> getInternalAccounts() {
		return internalAccounts;
	}

	public void setInternalAccounts(List<InternalAccount> internalAccounts) {
		this.internalAccounts = internalAccounts;
	}

	public List<Position> getPositions() {
		if (positions == null)
			positions = new ArrayList<Position>();
		return positions;
	}

	public void setPositions(List<Position> positions) {
		this.positions = positions;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getTotalContributionsToDate() {
		return totalContributionsToDate;
	}

	public void setTotalContributionsToDate(double totalContributionsToDate) {
		this.totalContributionsToDate = totalContributionsToDate;
	}

	public double getTotalContributionReasonContributionsToDate() {
		return totalContributionReasonContributionsToDate;
	}

	public void setTotalContributionReasonContributionsToDate(
			double totalContributionReasonContributionsToDate) {
		this.totalContributionReasonContributionsToDate = totalContributionReasonContributionsToDate;

	}

	public AddressHistory getCheckRecipientAddress() {
		if (checkRecipientAddress == null)
			checkRecipientAddress = new AddressHistory();
		return checkRecipientAddress;
	}

	public void setCheckRecipientAddress(AddressHistory checkRecipientAddress) {
		this.checkRecipientAddress = checkRecipientAddress;
	}

	public String getJournalDepartmentDescription() {
		return journalDepartmentDescription;
	}

	public void setJournalDepartmentDescription(
			String journalDepartmentDescription) {
		this.journalDepartmentDescription = journalDepartmentDescription;
	}

	public String getDocumentationRequired() {
		return documentationRequired;
	}

	public void setDocumentationRequired(String documentationRequired) {
		this.documentationRequired = documentationRequired;
	}

	public @BusinessType("com.lpl.virtualdomains.RequestType")String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public @BusinessType("com.lpl.virtualdomains.TransactionType") String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getMarginOptIn() {
		return marginOptIn;
	}

	public void setMarginOptIn(String marginOptIn) {
		this.marginOptIn = marginOptIn;
	}

	public String getOwnershipOfFunds() {
		return ownershipOfFunds;
	}

	public void setOwnershipOfFunds(String ownershipOfFunds) {
		this.ownershipOfFunds = ownershipOfFunds;
	}

	public String getDestinationOfFunds() {
		return destinationOfFunds;
	}

	public void setDestinationOfFunds(String destinationOfFunds) {
		this.destinationOfFunds = destinationOfFunds;
	}

	public String getRecipientOfFunds() {
		return recipientOfFunds;
	}

	public void setRecipientOfFunds(String recipientOfFunds) {
		this.recipientOfFunds = recipientOfFunds;
	}

	public String getTransactionReason() {
		return transactionReason;
	}

	public void setTransactionReason(String transactionReason) {
		this.transactionReason = transactionReason;
	}

	public String getJournalType() {
		return journalType;
	}

	public void setJournalType(String journalType) {
		this.journalType = journalType;
	}

	public String getCheckDeliveryMethod() {
		return checkDeliveryMethod;
	}

	public void setCheckDeliveryMethod(String checkDeliveryMethod) {
		this.checkDeliveryMethod = checkDeliveryMethod;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getMarginAgreement() {
		return marginAgreement;
	}

	public void setMarginAgreement(String marginAgreement) {
		this.marginAgreement = marginAgreement;
	}

	public String getTypeOfFunds() {
		return typeOfFunds;
	}

	public void setTypeOfFunds(String typeOfFunds) {
		this.typeOfFunds = typeOfFunds;
	}

	public String getTransactionRequires() {
		return transactionRequires;
	}

	public void setTransactionRequires(String transactionRequires) {
		this.transactionRequires = transactionRequires;
	}

	public String getSPADNotes() {
		return SPADNotes;
	}

	public void setSPADNotes(String sPADNotes) {
		SPADNotes = sPADNotes;
	}

	public String getRepId() {
		return repId;
	}

	public void setRepId(String repId) {
		this.repId = repId;
	}

	public int getTaxYearContributionPeriod() {
		return taxYearContributionPeriod;
	}

	public void setTaxYearContributionPeriod(int taxYearContributionPeriod) {
		this.taxYearContributionPeriod = taxYearContributionPeriod;
	}

	public String getW4PForm() {
		return w4PForm;
	}

	public void setW4PForm(String w4pForm) {
		w4PForm = w4pForm;
	}

	public String getSelectedNetGrossDistribution() {
		return selectedNetGrossDistribution;
	}

	public void setSelectedNetGrossDistribution(String selectedNetGrossDistribution) {
		this.selectedNetGrossDistribution = selectedNetGrossDistribution;
	}

	public String getCheckDeliveryAddress() {
		return checkDeliveryAddress;
	}

	public void setCheckDeliveryAddress(String checkDeliveryAddress) {
		this.checkDeliveryAddress = checkDeliveryAddress;
	}
	

}
