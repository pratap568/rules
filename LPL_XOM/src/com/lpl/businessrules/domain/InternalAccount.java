package com.lpl.businessrules.domain;


import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



public class InternalAccount {

	private String accountId;
	private String accountSubId;
	private String delivering_Receiving_AccountType;
	private String institutionCode;
	private String employeeClassCode;
	private String accountLocationCode;
	private String accountType;
	private String iraType;
	private String registrationType;
	private String marginSignedAgreement;
	private String optionSignedAgreement;
	private String productOrAccountClassCode;
	private Date accountOpeningDate;
	private Date accountClosedDate;
	private String houseAccountType;
	private AddressHistory registrationAddress;
	private String addressChangeCode;
	private String stateTaxWithHoldingIndicator;
	private double stateTaxWitholdingPercentage;
	private String federalTaxWithHoldingIndicator;
	private double federalTaxWithHoldingPercentage;
	private String dvpAccountStatus;
	private String cashStandingInstructionCode;
	private String annualFeePrePaid;
	private String toadFlag;
	private TransactionHistory recentContributions;
	//
	private Advisor advisor;
	private List<DOCADocuments> documentation;
	private List<Restrictions> restrictions;
	private List<Customer> customers;
	private List<AccountBalances> accountBalances;
	//

	private String institutionName = "";
	private long accountNumber;
	private String productGroup;
	private String ProductProgramType;
	private String productName;
	private double contributionLimit;
	private String accountDistribution;
	private String accountStatus;
	private String accountAddress;

	private String qrp_403b;
	private double terminationFee;
	private double annualMaintenanceFee;
	private double closingCostAssociateWithAccount;
	private String productClass;
	private String employeeClass;

	public InternalAccount() {
		accountId = "";
		accountSubId = "";
		institutionCode = "";
		employeeClassCode = "";
		accountLocationCode = "";
		accountType = "";
		registrationType = "";
		productOrAccountClassCode = "";
		accountOpeningDate = new Date();
		accountClosedDate = new Date();
		houseAccountType = "";
		addressChangeCode = "";
		stateTaxWithHoldingIndicator = "";
		stateTaxWitholdingPercentage = 0.0d;
		federalTaxWithHoldingIndicator = "";
		federalTaxWithHoldingPercentage = 0.0d;
		dvpAccountStatus = "";
		cashStandingInstructionCode = "";
		recentContributions = new TransactionHistory();

		advisor = new Advisor();
		documentation = new ArrayList<DOCADocuments>();
		restrictions = new ArrayList<Restrictions>();
		customers = new ArrayList<Customer>();
		accountBalances = new ArrayList<AccountBalances>();

		institutionName = "";
		accountNumber = 0L;
		productName = "";
		contributionLimit = 0.0d;
		accountAddress = "";

		terminationFee = 0.0d;
		annualMaintenanceFee = 0.0d;
		productClass = "";
		employeeClass = "";
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountSubId() {
		return accountSubId;
	}

	public void setAccountSubId(String accountSubId) {
		this.accountSubId = accountSubId;
	}

	public String getInstitutionCode() {
		return institutionCode;
	}

	public void setInstitutionCode(String institutionCode) {
		this.institutionCode = institutionCode;
	}

	public String getEmployeeClassCode() {
		return employeeClassCode;
	}

	public void setEmployeeClassCode(String employeeClassCode) {
		this.employeeClassCode = employeeClassCode;
	}

	public String getAccountLocationCode() {
		return accountLocationCode;
	}

	public void setAccountLocationCode(String accountLocationCode) {
		this.accountLocationCode = accountLocationCode;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRegistrationType() {
		return registrationType;
	}

	public void setRegistrationType(String registrationType) {
		this.registrationType = registrationType;
	}

	public String getProductOrAccountClassCode() {
		return productOrAccountClassCode;
	}

	public void setProductOrAccountClassCode(String productOrAccountClassCode) {
		this.productOrAccountClassCode = productOrAccountClassCode;
	}

	public Date getAccountOpeningDate() {
		return accountOpeningDate;
	}

	public void setAccountOpeningDate(Date accountOpeningDate) {
		this.accountOpeningDate = accountOpeningDate;
	}

	public Date getAccountClosedDate() {
		return accountClosedDate;
	}

	public void setAccountClosedDate(Date accountClosedDate) {
		this.accountClosedDate = accountClosedDate;
	}

	public String getHouseAccountType() {
		return houseAccountType;
	}

	public void setHouseAccountType(String houseAccountType) {
		this.houseAccountType = houseAccountType;
	}

	public AddressHistory getRegistrationAddress() {
		if (registrationAddress == null)
			registrationAddress = new AddressHistory();
		return registrationAddress;
	}

	public void setRegistrationAddress(AddressHistory registrationAddress) {
		this.registrationAddress = registrationAddress;
	}

	public String getAddressChangeCode() {
		return addressChangeCode;
	}

	public void setAddressChangeCode(String addressChangeCode) {
		this.addressChangeCode = addressChangeCode;
	}

	public String getStateTaxWithHoldingIndicator() {
		return stateTaxWithHoldingIndicator;
	}

	public void setStateTaxWithHoldingIndicator(
			String stateTaxWithHoldingIndicator) {
		this.stateTaxWithHoldingIndicator = stateTaxWithHoldingIndicator;
	}

	public double getStateTaxWitholdingPercentage() {
		return stateTaxWitholdingPercentage;
	}

	public void setStateTaxWitholdingPercentage(
			double stateTaxWitholdingPercentage) {
		this.stateTaxWitholdingPercentage = stateTaxWitholdingPercentage;
	}

	public String getFederalTaxWithHoldingIndicator() {
		return federalTaxWithHoldingIndicator;
	}

	public void setFederalTaxWithHoldingIndicator(
			String federalTaxWithHoldingIndicator) {
		this.federalTaxWithHoldingIndicator = federalTaxWithHoldingIndicator;
	}

	public double getFederalTaxWithHoldingPercentage() {
		return federalTaxWithHoldingPercentage;
	}

	public void setFederalTaxWithHoldingPercentage(
			double federalTaxWithHoldingPercentage) {
		this.federalTaxWithHoldingPercentage = federalTaxWithHoldingPercentage;
	}

	public String getDvpAccountStatus() {
		return dvpAccountStatus;
	}

	public void setDvpAccountStatus(String dvpAccountStatus) {
		this.dvpAccountStatus = dvpAccountStatus;
	}

	public String getCashStandingInstructionCode() {
		return cashStandingInstructionCode;
	}

	public void setCashStandingInstructionCode(
			String cashStandingInstructionCode) {
		this.cashStandingInstructionCode = cashStandingInstructionCode;
	}

	public TransactionHistory getRecentContributions() {
		if (recentContributions == null)
			recentContributions = new TransactionHistory();
		return recentContributions;
	}

	public void setRecentContributions(TransactionHistory recentContributions) {
		this.recentContributions = recentContributions;
	}

	public Advisor getAdvisor() {
		if (advisor == null)
			advisor = new Advisor();
		return advisor;
	}

	public void setAdvisor(Advisor advisor) {
		this.advisor = advisor;
	}

	public List<DOCADocuments> getDocumentation() {
		if (documentation == null)
			documentation = new ArrayList<DOCADocuments>();
		return documentation;
	}

	public void setDocumentation(List<DOCADocuments> documentation) {
		this.documentation = documentation;
	}

	public List<Restrictions> getRestrictions() {
		if (restrictions == null)
			restrictions = new ArrayList<Restrictions>();
		return restrictions;
	}

	public void setRestrictions(List<Restrictions> restrictions) {
		this.restrictions = restrictions;
	}

	public List<Customer> getCustomers() {
		if (customers == null)
			customers = new ArrayList<Customer>();
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	public List<AccountBalances> getAccountBalances() {
		if (accountBalances == null)
			accountBalances = new ArrayList<AccountBalances>();
		return accountBalances;
	}

	public void setAccountBalances(List<AccountBalances> accountBalances) {
		this.accountBalances = accountBalances;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public double getContributionLimit() {
		return contributionLimit;
	}

	public void setContributionLimit(double contributionLimit) {
		this.contributionLimit = contributionLimit;
	}

	public String getAccountAddress() {
		return accountAddress;
	}

	public void setAccountAddress(String accountAddress) {
		this.accountAddress = accountAddress;
	}

	public double getTerminationFee() {
		return terminationFee;
	}

	public void setTerminationFee(double terminationFee) {
		this.terminationFee = terminationFee;
	}

	public double getAnnualMaintenanceFee() {
		return annualMaintenanceFee;
	}

	public void setAnnualMaintenanceFee(double annualMaintenanceFee) {
		this.annualMaintenanceFee = annualMaintenanceFee;
	}

	public double getClosingCostAssociateWithAccount() {
		return closingCostAssociateWithAccount;
	}

	public void setClosingCostAssociateWithAccount(
			double closingCostAssociateWithAccount) {
		this.closingCostAssociateWithAccount = closingCostAssociateWithAccount;
	}

	public String getProductClass() {
		return productClass;
	}

	public void setProductClass(String productClass) {
		this.productClass = productClass;
	}

	public String getEmployeeClass() {
		return employeeClass;
	}

	public void setEmployeeClass(String employeeClass) {
		this.employeeClass = employeeClass;
	}

	public String getDelivering_Receiving_AccountType() {
		return delivering_Receiving_AccountType;
	}

	public void setDelivering_Receiving_AccountType(
			String delivering_Receiving_AccountType) {
		this.delivering_Receiving_AccountType = delivering_Receiving_AccountType;
	}

	public String getIraType() {
		return iraType;
	}

	public void setIraType(String iraType) {
		this.iraType = iraType;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getMarginSignedAgreement() {
		return marginSignedAgreement;
	}

	public void setMarginSignedAgreement(String marginSignedAgreement) {
		this.marginSignedAgreement = marginSignedAgreement;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getOptionSignedAgreement() {
		return optionSignedAgreement;
	}

	public void setOptionSignedAgreement(String optionSignedAgreement) {
		this.optionSignedAgreement = optionSignedAgreement;
	}

	public String getAnnualFeePrePaid() {
		return annualFeePrePaid;
	}

	public void setAnnualFeePrePaid(String annualFeePrePaid) {
		this.annualFeePrePaid = annualFeePrePaid;
	}

	public String getToadFlag() {
		return toadFlag;
	}

	public void setToadFlag(String toadFlag) {
		this.toadFlag = toadFlag;
	}

	public String getProductGroup() {
		return productGroup;
	}

	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}

	public String getProductProgramType() {
		return ProductProgramType;
	}

	public void setProductProgramType(String productProgramType) {
		ProductProgramType = productProgramType;
	}

	public String getAccountDistribution() {
		return accountDistribution;
	}

	public void setAccountDistribution(String accountDistribution) {
		this.accountDistribution = accountDistribution;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getQrp_403b() {
		return qrp_403b;
	}

	public void setQrp_403b(String qrp_403b) {
		this.qrp_403b = qrp_403b;
	}

}
