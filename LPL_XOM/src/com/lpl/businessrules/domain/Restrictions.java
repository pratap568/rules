package com.lpl.businessrules.domain;

public class Restrictions {

	private String doNoMoreBusinessFlag;
	private String blockedCountries;

	private String legalRestrictionFlag;
	private String riskOrReviewFlag;
	private String ACATDeliveryFlag;
	private String CMPRestrictionFlag;
	private String CMPWarningFlag;
	private String combinedAccountFlag;
	private String guaranteedAccountFlag;
	private String holdAssetsFlg;
	private String mailReturnFlag;
	private String notedOfDeathFlag;
	private String noticeOfDivorceFlag;
	private String pledgeAccountFlag;
	private String securityPasswordFlag;
	private String generalRestrictionFlag;

	private String escheatmentFlag;
	private String ofacResponse;

	private String awayLocateViolationFlag;

	public Restrictions() {

	}

	public String getDoNoMoreBusinessFlag() {
		return doNoMoreBusinessFlag;
	}

	public void setDoNoMoreBusinessFlag(String doNoMoreBusinessFlag) {
		this.doNoMoreBusinessFlag = doNoMoreBusinessFlag;
	}

	public String getBlockedCountries() {
		return blockedCountries;
	}

	public void setBlockedCountries(String blockedCountries) {
		this.blockedCountries = blockedCountries;
	}

	public String getLegalRestrictionFlag() {
		return legalRestrictionFlag;
	}

	public void setLegalRestrictionFlag(String legalRestrictionFlag) {
		this.legalRestrictionFlag = legalRestrictionFlag;
	}

	public String getRiskOrReviewFlag() {
		return riskOrReviewFlag;
	}

	public void setRiskOrReviewFlag(String riskOrReviewFlag) {
		this.riskOrReviewFlag = riskOrReviewFlag;
	}

	public String getACATDeliveryFlag() {
		return ACATDeliveryFlag;
	}

	public void setACATDeliveryFlag(String aCATDeliveryFlag) {
		ACATDeliveryFlag = aCATDeliveryFlag;
	}

	public String getCMPRestrictionFlag() {
		return CMPRestrictionFlag;
	}

	public void setCMPRestrictionFlag(String cMPRestrictionFlag) {
		CMPRestrictionFlag = cMPRestrictionFlag;
	}

	public String getCMPWarningFlag() {
		return CMPWarningFlag;
	}

	public void setCMPWarningFlag(String cMPWarningFlag) {
		CMPWarningFlag = cMPWarningFlag;
	}

	public String getCombinedAccountFlag() {
		return combinedAccountFlag;
	}

	public void setCombinedAccountFlag(String combinedAccountFlag) {
		this.combinedAccountFlag = combinedAccountFlag;
	}

	public String getGuaranteedAccountFlag() {
		return guaranteedAccountFlag;
	}

	public void setGuaranteedAccountFlag(String guaranteedAccountFlag) {
		this.guaranteedAccountFlag = guaranteedAccountFlag;
	}

	public String getHoldAssetsFlg() {
		return holdAssetsFlg;
	}

	public void setHoldAssetsFlg(String holdAssetsFlg) {
		this.holdAssetsFlg = holdAssetsFlg;
	}

	public String getMailReturnFlag() {
		return mailReturnFlag;
	}

	public void setMailReturnFlag(String mailReturnFlag) {
		this.mailReturnFlag = mailReturnFlag;
	}

	public String getNotedOfDeathFlag() {
		return notedOfDeathFlag;
	}

	public void setNotedOfDeathFlag(String notedOfDeathFlag) {
		this.notedOfDeathFlag = notedOfDeathFlag;
	}

	public String getNoticeOfDivorceFlag() {
		return noticeOfDivorceFlag;
	}

	public void setNoticeOfDivorceFlag(String noticeOfDivorceFlag) {
		this.noticeOfDivorceFlag = noticeOfDivorceFlag;
	}

	public String getPledgeAccountFlag() {
		return pledgeAccountFlag;
	}

	public void setPledgeAccountFlag(String pledgeAccountFlag) {
		this.pledgeAccountFlag = pledgeAccountFlag;
	}

	public String getSecurityPasswordFlag() {
		return securityPasswordFlag;
	}

	public void setSecurityPasswordFlag(String securityPasswordFlag) {
		this.securityPasswordFlag = securityPasswordFlag;
	}

	public String getGeneralRestrictionFlag() {
		return generalRestrictionFlag;
	}

	public void setGeneralRestrictionFlag(String generalRestrictionFlag) {
		this.generalRestrictionFlag = generalRestrictionFlag;
	}

	public String getEscheatmentFlag() {
		return escheatmentFlag;
	}

	public void setEscheatmentFlag(String escheatmentFlag) {
		this.escheatmentFlag = escheatmentFlag;
	}

	public String getOfacResponse() {
		return ofacResponse;
	}

	public void setOfacResponse(String ofacResponse) {
		this.ofacResponse = ofacResponse;
	}

	public String getAwayLocateViolationFlag() {
		return awayLocateViolationFlag;
	}

	public void setAwayLocateViolationFlag(String awayLocateViolationFlag) {
		this.awayLocateViolationFlag = awayLocateViolationFlag;
	}

}
