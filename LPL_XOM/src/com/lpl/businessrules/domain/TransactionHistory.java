package com.lpl.businessrules.domain;

import java.util.Date;

public class TransactionHistory {
	private String transactionId="";
	private String transactionType="";
	private Date transactionDate;
	private double transactionAmount=0.0d;
	private String transactionSourceCode="";
	private String transactionPECOCode="";
	private double recentContributionsFor5Days=0.0d;
	private double recentContributionsFor30Days=0.0d;
	private Date year;
	
	
	
	public double getRecentContributionsFor5Days() {
		return recentContributionsFor5Days;
	}
	public void setRecentContributionsFor5Days(double recentContributionsFor5Days) {
		this.recentContributionsFor5Days = recentContributionsFor5Days;
	}
	public double getRecentContributionsFor30Days() {
		return recentContributionsFor30Days;
	}
	public void setRecentContributionsFor30Days(double recentContributionsFor30Days) {
		this.recentContributionsFor30Days = recentContributionsFor30Days;
	}
	public Date getYear() {
		return year;
	}
	public void setYear(Date year) {
		this.year = year;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public double getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(double transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionSourceCode() {
		return transactionSourceCode;
	}
	public void setTransactionSourceCode(String transactionSourceCode) {
		this.transactionSourceCode = transactionSourceCode;
	}
	public String getTransactionPECOCode() {
		return transactionPECOCode;
	}
	public void setTransactionPECOCode(String transactionPECOCode) {
		this.transactionPECOCode = transactionPECOCode;
	}

}
