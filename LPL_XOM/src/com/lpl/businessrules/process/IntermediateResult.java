package com.lpl.businessrules.process;

import ilog.rules.bom.annotations.BusinessType;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lpl.businessrules.domain.DOCADocuments;

public class IntermediateResult {

	private String deliveringAccountClassCodeEligibility = "";
	private String receivingAccountClassCodeEligibility = "";
	private String accountProtectedType = "";
	private String accountRiskLevel = "";
	private String accountTaxWithHoldingInstructionsOnFile = "";
	private String accountTransferredStatus = "";
	private String activityRiskLevel = "";
	private String addressChangeWithInLast30Days = "";
	private String advisorAffliation = "";
	private String advisorRiskLevel = "";
	private List<String> blockedCountryList = new ArrayList<>();
	private String bondEligibility = "";
	private String bondMarginable = "";
	private String businessAddressCheck = "";
	private String businessEligibilityRestrictions = "";
	private String checkDeliveryAddressOnBlockedCountryList = "";
	private String checkDisbursementEligibility = "";
	private String checkPurposeLine = "";
	private String collateralAccount = "";
	private String contributionLimitbeenExceeded = "";
	private double contributionLimitByReason = 0.0d;
	private String contributionOccuredInSelectedTaxPeriod = "";
	private String custodiedAtLPL = "";
	private int deadlineDate = 0;
	private String deadlineMonth = "";
	private Date deadlineYear;
	private String deathCertificateOnFile = "";
	private String deathJournalRisk = "";
	private String deliveringAccountClassCodeEligibilityJournals = "";
	private String deliveringAccountRetailOrRetirement = "";
	private String receivingAccountRetailOrRetirement = "";
	private String DOCADocumentsInGoodOrder = "";
	private String docaStatusCode = "";
	private String eligibilityAccountPotectedType = "";
	private String eligibilityBORDRestrictions = "";
	private String extendedRequestType = "";
	private String firstOrThirdPartyRequest = "";
	private String foreignAddressCheck = "";
	private String guardianshipAccount = "";
	private String homeAddressCheck = "";
	private String houseAccountCheck = "";
	private String journalActivityRisk = "";
	private String journalAmountSuitsMinimumDenominations = "";
	private String journalEligibility = "";
	private String mailCode = "";
	private String marginability = "";
	private String marginCashAvailableExceeded = "";
	private String marginEligibility = "";
	private String marginPriceMinimumRequirementMet = "";
	private double marginTransactionAmount = 0.0d;
	private String marginTransactionEligibility = "";
	private String matchingBranchId = "";
	private double minimumDenominations = 0.0d;
	private String minimumPriceMet = "";
	private String marginActivityRisk = "";
	private String OBAAddressCheck = "";
	private String positionMarginability = "";
	private String receivingAccountClassCodeEligibilityJournals = "";
	private String recentDepositsImpact = "";
	private String redFlags = "";
	private String representativeZ_Coded = "";
	private String requestRequireMarginCash = "";
	private String requestRequireMarginSecurities = "";
	private double requestSpecificFee = 0.0d;
	private List<DOCADocuments> requiredDOCADocuments;
	private String requireMarginCashOrSecurities = "";
	private String riskLevelBORDRestrictions = "";
	private String sameOrDifferentAccountID = "";
	private String securityEligibility = "";
	private String securityInGoodOrder = "";
	private String securityLocationIGO = "";
	private String taxHolidayObservation = "";
	private Date taxPeriodDeadlineDate;
	private Date taxPeriodStartDate;
	private String TOPCode = "";
	private double totalBalanceAddition = 0.0d;
	private double totalBalanceSubtraction = 0.0d;
	private double totalContributionLimit;
	private String transactionEligibility = "";
	private String validReasonForContribution = "";
	private String validReasonForDistribution = "";
	private String validReasonForJCW = "";
	private double recentDeposits = 0.0d;
	private String netDistributionPECOCode = "";
	private String federalPECOCode = "";
	private String statePECOCode = "";
	private String contributionPECOCode = "";
	private String sourceCodes = "";
	private String qualifiedRothDistribution = "";
	private String stateTaxWithHoldingMinimumRequirements = "";
	private String stateGeneralLedger = "";
	private String federalGeneralLedger = "";
	private double allowedIncrements = 0.0d;
	private String journalSecurityEligibility = "";
	private String w4PFormIGO = "";
	private double stateTaxWitholdingPercentage = 0.0d;
	private double actualTransactionAmount = 0.0d;
	private String managedAccountEligiblity = "";
	private String Cash = "";
	private List<DOCADocuments> documentCodes;

	public IntermediateResult() {
		extendedRequestType = "";
		requestSpecificFee = 0.0d;
		requireMarginCashOrSecurities = "";
		totalContributionLimit = 0.0d;
		deathCertificateOnFile = "";
		requiredDOCADocuments = new ArrayList<>();
		documentCodes = new ArrayList<DOCADocuments>();
		deliveringAccountRetailOrRetirement = "";
		receivingAccountRetailOrRetirement = "";

	}

	public String getDeliveringAccountClassCodeEligibility() {
		return deliveringAccountClassCodeEligibility;
	}

	public void setDeliveringAccountClassCodeEligibility(
			String deliveringAccountClassCodeEligibility) {
		this.deliveringAccountClassCodeEligibility = deliveringAccountClassCodeEligibility;
	}

	public String getReceivingAccountClassCodeEligibility() {
		return receivingAccountClassCodeEligibility;
	}

	public void setReceivingAccountClassCodeEligibility(
			String receivingAccountClassCodeEligibility) {
		this.receivingAccountClassCodeEligibility = receivingAccountClassCodeEligibility;
	}

	public String getAccountProtectedType() {
		return accountProtectedType;
	}

	public void setAccountProtectedType(String accountProtectedType) {
		this.accountProtectedType = accountProtectedType;
	}

	public String getAccountRiskLevel() {
		return accountRiskLevel;
	}

	public void setAccountRiskLevel(String accountRiskLevel) {
		this.accountRiskLevel = accountRiskLevel;
	}

	public String getAccountTaxWithHoldingInstructionsOnFile() {
		return accountTaxWithHoldingInstructionsOnFile;
	}

	public void setAccountTaxWithHoldingInstructionsOnFile(
			String accountTaxWithHoldingInstructionsOnFile) {
		this.accountTaxWithHoldingInstructionsOnFile = accountTaxWithHoldingInstructionsOnFile;
	}

	public String getAccountTransferredStatus() {
		return accountTransferredStatus;
	}

	public void setAccountTransferredStatus(String accountTransferredStatus) {
		this.accountTransferredStatus = accountTransferredStatus;
	}

	public String getActivityRiskLevel() {
		return activityRiskLevel;
	}

	public void setActivityRiskLevel(String activityRiskLevel) {
		this.activityRiskLevel = activityRiskLevel;
	}

	public String getAddressChangeWithInLast30Days() {
		return addressChangeWithInLast30Days;
	}

	public void setAddressChangeWithInLast30Days(
			String addressChangeWithInLast30Days) {
		this.addressChangeWithInLast30Days = addressChangeWithInLast30Days;
	}

	public String getAdvisorAffliation() {
		return advisorAffliation;
	}

	public void setAdvisorAffliation(String advisorAffliation) {
		this.advisorAffliation = advisorAffliation;
	}

	public String getAdvisorRiskLevel() {
		return advisorRiskLevel;
	}

	public void setAdvisorRiskLevel(String advisorRiskLevel) {
		this.advisorRiskLevel = advisorRiskLevel;
	}

	public List<String> getBlockedCountryList() {
		return blockedCountryList;
	}

	public void setBlockedCountryList(List<String> blockedCountryList) {
		this.blockedCountryList = blockedCountryList;
	}

	public @BusinessType("com.lpl.virtualdomains.BondEligibility") String getBondEligibility() {
		return bondEligibility;
	}

	public void setBondEligibility(String bondEligibility) {
		this.bondEligibility = bondEligibility;
	}

	public @BusinessType("com.lpl.virtualdomains.BondMarginability") String getBondMarginable() {
		return bondMarginable;
	}

	public void setBondMarginable(String bondMarginable) {
		this.bondMarginable = bondMarginable;
	}

	public String getBusinessAddressCheck() {
		return businessAddressCheck;
	}

	public void setBusinessAddressCheck(String businessAddressCheck) {
		this.businessAddressCheck = businessAddressCheck;
	}

	public String getBusinessEligibilityRestrictions() {
		return businessEligibilityRestrictions;
	}

	public void setBusinessEligibilityRestrictions(
			String businessEligibilityRestrictions) {
		this.businessEligibilityRestrictions = businessEligibilityRestrictions;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getCheckDeliveryAddressOnBlockedCountryList() {
		return checkDeliveryAddressOnBlockedCountryList;
	}

	public void setCheckDeliveryAddressOnBlockedCountryList(
			String checkDeliveryAddressOnBlockedCountryList) {
		this.checkDeliveryAddressOnBlockedCountryList = checkDeliveryAddressOnBlockedCountryList;
	}

	public @BusinessType("com.lpl.virtualdomains.BondEligibility") String getCheckDisbursementEligibility() {
		return checkDisbursementEligibility;
	}

	public void setCheckDisbursementEligibility(
			String checkDisbursementEligibility) {
		this.checkDisbursementEligibility = checkDisbursementEligibility;
	}

	public String getCheckPurposeLine() {
		return checkPurposeLine;
	}

	public void setCheckPurposeLine(String checkPurposeLine) {
		this.checkPurposeLine = checkPurposeLine;
	}

	public String getCollateralAccount() {
		return collateralAccount;
	}

	public void setCollateralAccount(String collateralAccount) {
		this.collateralAccount = collateralAccount;
	}

	public String getContributionLimitbeenExceeded() {
		return contributionLimitbeenExceeded;
	}

	public void setContributionLimitbeenExceeded(
			String contributionLimitbeenExceeded) {
		this.contributionLimitbeenExceeded = contributionLimitbeenExceeded;
	}

	public double getContributionLimitByReason() {
		return contributionLimitByReason;
	}

	public void setContributionLimitByReason(double contributionLimitByReason) {
		this.contributionLimitByReason = contributionLimitByReason;
	}

	public String getContributionOccuredInSelectedTaxPeriod() {
		return contributionOccuredInSelectedTaxPeriod;
	}

	public void setContributionOccuredInSelectedTaxPeriod(
			String contributionOccuredInSelectedTaxPeriod) {
		this.contributionOccuredInSelectedTaxPeriod = contributionOccuredInSelectedTaxPeriod;
	}

	public String getCustodiedAtLPL() {
		return custodiedAtLPL;
	}

	public void setCustodiedAtLPL(String custodiedAtLPL) {
		this.custodiedAtLPL = custodiedAtLPL;
	}

	public int getDeadlineDate() {
		return deadlineDate;
	}

	public void setDeadlineDate(int deadlineDate) {
		this.deadlineDate = deadlineDate;
	}

	public String getDeadlineMonth() {
		return deadlineMonth;
	}

	public void setDeadlineMonth(String deadlineMonth) {
		this.deadlineMonth = deadlineMonth;
	}

	public Date getDeadlineYear() {
		return deadlineYear;
	}

	public void setDeadlineYear(Date deadlineYear) {
		this.deadlineYear = deadlineYear;
	}

	public String getDeathCertificateOnFile() {
		return deathCertificateOnFile;
	}

	public void setDeathCertificateOnFile(String deathCertificateOnFile) {
		this.deathCertificateOnFile = deathCertificateOnFile;
	}

	public String getDeathJournalRisk() {
		return deathJournalRisk;
	}

	public void setDeathJournalRisk(String deathJournalRisk) {
		this.deathJournalRisk = deathJournalRisk;
	}

	public String getDeliveringAccountClassCodeEligibilityJournals() {
		return deliveringAccountClassCodeEligibilityJournals;
	}

	public void setDeliveringAccountClassCodeEligibilityJournals(
			String deliveringAccountClassCodeEligibilityJournals) {
		this.deliveringAccountClassCodeEligibilityJournals = deliveringAccountClassCodeEligibilityJournals;
	}

	public String getDOCADocumentsInGoodOrder() {
		return DOCADocumentsInGoodOrder;
	}

	public void setDOCADocumentsInGoodOrder(String dOCADocumentsInGoodOrder) {
		DOCADocumentsInGoodOrder = dOCADocumentsInGoodOrder;
	}

	public String getDocaStatusCode() {
		return docaStatusCode;
	}

	public void setDocaStatusCode(String docaStatusCode) {
		this.docaStatusCode = docaStatusCode;
	}

	public String getEligibilityAccountPotectedType() {
		return eligibilityAccountPotectedType;
	}

	public void setEligibilityAccountPotectedType(
			String eligibilityAccountPotectedType) {
		this.eligibilityAccountPotectedType = eligibilityAccountPotectedType;
	}

	public String getEligibilityBORDRestrictions() {
		return eligibilityBORDRestrictions;
	}

	public void setEligibilityBORDRestrictions(
			String eligibilityBORDRestrictions) {
		this.eligibilityBORDRestrictions = eligibilityBORDRestrictions;
	}

	public @BusinessType("com.lpl.virtualdomains.ExtendedRequestType") String getExtendedRequestType() {
		return extendedRequestType;
	}

	public void setExtendedRequestType(String extendedRequestType) {
		this.extendedRequestType = extendedRequestType;
	}

	public String getFirstOrThirdPartyRequest() {
		return firstOrThirdPartyRequest;
	}

	public void setFirstOrThirdPartyRequest(String firstOrThirdPartyRequest) {
		this.firstOrThirdPartyRequest = firstOrThirdPartyRequest;
	}

	public String getForeignAddressCheck() {
		return foreignAddressCheck;
	}

	public void setForeignAddressCheck(String foreignAddressCheck) {
		this.foreignAddressCheck = foreignAddressCheck;
	}

	public String getGuardianshipAccount() {
		return guardianshipAccount;
	}

	public void setGuardianshipAccount(String guardianshipAccount) {
		this.guardianshipAccount = guardianshipAccount;
	}

	public String getHomeAddressCheck() {
		return homeAddressCheck;
	}

	public void setHomeAddressCheck(String homeAddressCheck) {
		this.homeAddressCheck = homeAddressCheck;
	}

	public String getHouseAccountCheck() {
		return houseAccountCheck;
	}

	public void setHouseAccountCheck(String houseAccountCheck) {
		this.houseAccountCheck = houseAccountCheck;
	}

	public String getJournalActivityRisk() {
		return journalActivityRisk;
	}

	public void setJournalActivityRisk(String journalActivityRisk) {
		this.journalActivityRisk = journalActivityRisk;
	}

	public String getJournalAmountSuitsMinimumDenominations() {
		return journalAmountSuitsMinimumDenominations;
	}

	public void setJournalAmountSuitsMinimumDenominations(
			String journalAmountSuitsMinimumDenominations) {
		this.journalAmountSuitsMinimumDenominations = journalAmountSuitsMinimumDenominations;
	}

	public @BusinessType("com.lpl.virtualdomains.BondEligibility") String getJournalEligibility() {
		return journalEligibility;
	}

	public void setJournalEligibility(String journalEligibility) {
		this.journalEligibility = journalEligibility;
	}

	public String getMailCode() {
		return mailCode;
	}

	public void setMailCode(String mailCode) {
		this.mailCode = mailCode;
	}

	public String getMarginability() {
		return marginability;
	}

	public void setMarginability(String marginability) {
		this.marginability = marginability;
	}

	public String getMarginCashAvailableExceeded() {
		return marginCashAvailableExceeded;
	}

	public void setMarginCashAvailableExceeded(
			String marginCashAvailableExceeded) {
		this.marginCashAvailableExceeded = marginCashAvailableExceeded;
	}

	public String getMarginEligibility() {
		return marginEligibility;
	}

	public void setMarginEligibility(String marginEligibility) {
		this.marginEligibility = marginEligibility;
	}

	public String getMarginPriceMinimumRequirementMet() {
		return marginPriceMinimumRequirementMet;
	}

	public void setMarginPriceMinimumRequirementMet(
			String marginPriceMinimumRequirementMet) {
		this.marginPriceMinimumRequirementMet = marginPriceMinimumRequirementMet;
	}

	public double getMarginTransactionAmount() {
		return marginTransactionAmount;
	}

	public void setMarginTransactionAmount(double marginTransactionAmount) {
		this.marginTransactionAmount = marginTransactionAmount;
	}

	public @BusinessType("com.lpl.virtualdomains.BondEligibility")  String getMarginTransactionEligibility() {
		return marginTransactionEligibility;
	}

	public void setMarginTransactionEligibility(
			String marginTransactionEligibility) {
		this.marginTransactionEligibility = marginTransactionEligibility;
	}

	public @BusinessType("com.lpl.virtualdomains.MatchingBranchId") String getMatchingBranchId() {
		return matchingBranchId;
	}

	public void setMatchingBranchId(String matchingBranchId) {
		this.matchingBranchId = matchingBranchId;
	}

	public double getMinimumDenominations() {
		return minimumDenominations;
	}

	public void setMinimumDenominations(double minimumDenominations) {
		this.minimumDenominations = minimumDenominations;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getMinimumPriceMet() {
		return minimumPriceMet;
	}

	public void setMinimumPriceMet(String minimumPriceMet) {
		this.minimumPriceMet = minimumPriceMet;
	}

	public String getMarginActivityRisk() {
		return marginActivityRisk;
	}

	public void setMarginActivityRisk(String marginActivityRisk) {
		this.marginActivityRisk = marginActivityRisk;
	}

	public String getOBAAddressCheck() {
		return OBAAddressCheck;
	}

	public void setOBAAddressCheck(String oBAAddressCheck) {
		OBAAddressCheck = oBAAddressCheck;
	}

	public @BusinessType("com.lpl.virtualdomains.PositionMarginability") String getPositionMarginability() {
		return positionMarginability;
	}

	public void setPositionMarginability(String positionMarginability) {
		this.positionMarginability = positionMarginability;
	}

	public String getReceivingAccountClassCodeEligibilityJournals() {
		return receivingAccountClassCodeEligibilityJournals;
	}

	public void setReceivingAccountClassCodeEligibilityJournals(
			String receivingAccountClassCodeEligibilityJournals) {
		this.receivingAccountClassCodeEligibilityJournals = receivingAccountClassCodeEligibilityJournals;
	}

	public String getRecentDepositsImpact() {
		return recentDepositsImpact;
	}

	public void setRecentDepositsImpact(String recentDepositsImpact) {
		this.recentDepositsImpact = recentDepositsImpact;
	}

	public String getRedFlags() {
		return redFlags;
	}

	public void setRedFlags(String redFlags) {
		this.redFlags = redFlags;
	}

	public String getRepresentativeZ_Coded() {
		return representativeZ_Coded;
	}

	public void setRepresentativeZ_Coded(String representativeZ_Coded) {
		this.representativeZ_Coded = representativeZ_Coded;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getRequestRequireMarginCash() {
		return requestRequireMarginCash;
	}

	public void setRequestRequireMarginCash(String requestRequireMarginCash) {
		this.requestRequireMarginCash = requestRequireMarginCash;
	}

	public @BusinessType("com.lpl.virtualdomains.ValidationDomain") String getRequestRequireMarginSecurities() {
		return requestRequireMarginSecurities;
	}

	public void setRequestRequireMarginSecurities(
			String requestRequireMarginSecurities) {
		this.requestRequireMarginSecurities = requestRequireMarginSecurities;
	}

	public double getRequestSpecificFee() {
		return requestSpecificFee;
	}

	public void setRequestSpecificFee(double requestSpecificFee) {
		this.requestSpecificFee = requestSpecificFee;
	}

	public List<DOCADocuments> getRequiredDOCADocuments() {
		return requiredDOCADocuments;
	}

	public void setRequiredDOCADocuments(
			List<DOCADocuments> requiredDOCADocuments) {
		this.requiredDOCADocuments = requiredDOCADocuments;
	}

	public String getRequireMarginCashOrSecurities() {
		return requireMarginCashOrSecurities;
	}

	public void setRequireMarginCashOrSecurities(
			String requireMarginCashOrSecurities) {
		this.requireMarginCashOrSecurities = requireMarginCashOrSecurities;
	}

	public String getRiskLevelBORDRestrictions() {
		return riskLevelBORDRestrictions;
	}

	public void setRiskLevelBORDRestrictions(String riskLevelBORDRestrictions) {
		this.riskLevelBORDRestrictions = riskLevelBORDRestrictions;
	}

	public String getSameOrDifferentAccountID() {
		return sameOrDifferentAccountID;
	}

	public void setSameOrDifferentAccountID(String sameOrDifferentAccountID) {
		this.sameOrDifferentAccountID = sameOrDifferentAccountID;
	}

	public @BusinessType("com.lpl.virtualdomains.BondEligibility") String getSecurityEligibility() {
		return securityEligibility;
	}

	public void setSecurityEligibility(String securityEligibility) {
		this.securityEligibility = securityEligibility;
	}

	public @BusinessType("com.lpl.virtualdomains.SecurityLocationIGO") String getSecurityInGoodOrder() {
		return securityInGoodOrder;
	}

	public void setSecurityInGoodOrder(String securityInGoodOrder) {
		this.securityInGoodOrder = securityInGoodOrder;
	}

	public @BusinessType("com.lpl.virtualdomains.SecurityLocationIGO") String getSecurityLocationIGO() {
		return securityLocationIGO;
	}

	public void setSecurityLocationIGO(String securityLocationIGO) {
		this.securityLocationIGO = securityLocationIGO;
	}

	public String getTaxHolidayObservation() {
		return taxHolidayObservation;
	}

	public void setTaxHolidayObservation(String taxHolidayObservation) {
		this.taxHolidayObservation = taxHolidayObservation;
	}

	public Date getTaxPeriodDeadlineDate() {
		return taxPeriodDeadlineDate;
	}

	public void setTaxPeriodDeadlineDate(Date taxPeriodDeadlineDate) {
		this.taxPeriodDeadlineDate = taxPeriodDeadlineDate;
	}

	public Date getTaxPeriodStartDate() {
		return taxPeriodStartDate;
	}

	public void setTaxPeriodStartDate(Date taxPeriodStartDate) {
		this.taxPeriodStartDate = taxPeriodStartDate;
	}

	public String getTOPCode() {
		return TOPCode;
	}

	public void setTOPCode(String tOPCode) {
		TOPCode = tOPCode;
	}

	public double getTotalBalanceAddition() {
		return totalBalanceAddition;
	}

	public void setTotalBalanceAddition(double totalBalanceAddition) {
		this.totalBalanceAddition = totalBalanceAddition;
	}

	public double getTotalBalanceSubtraction() {
		return totalBalanceSubtraction;
	}

	public void setTotalBalanceSubtraction(double totalBalanceSubtraction) {
		this.totalBalanceSubtraction = totalBalanceSubtraction;
	}

	public double getTotalContributionLimit() {
		return totalContributionLimit;
	}

	public void setTotalContributionLimit(double totalContributionLimit) {
		this.totalContributionLimit = totalContributionLimit;
	}

	public String getTransactionEligibility() {
		return transactionEligibility;
	}

	public void setTransactionEligibility(String transactionEligibility) {
		this.transactionEligibility = transactionEligibility;
	}

	public String getValidReasonForContribution() {
		return validReasonForContribution;
	}

	public void setValidReasonForContribution(String validReasonForContribution) {
		this.validReasonForContribution = validReasonForContribution;
	}

	public String getValidReasonForDistribution() {
		return validReasonForDistribution;
	}

	public void setValidReasonForDistribution(String validReasonForDistribution) {
		this.validReasonForDistribution = validReasonForDistribution;
	}

	public String getValidReasonForJCW() {
		return validReasonForJCW;
	}

	public void setValidReasonForJCW(String validReasonForJCW) {
		this.validReasonForJCW = validReasonForJCW;
	}

	public double getRecentDeposits() {
		return recentDeposits;
	}

	public void setRecentDeposits(double recentDeposits) {
		this.recentDeposits = recentDeposits;
	}

	public String getNetDistributionPECOCode() {
		return netDistributionPECOCode;
	}

	public void setNetDistributionPECOCode(String netDistributionPECOCode) {
		this.netDistributionPECOCode = netDistributionPECOCode;
	}

	public String getFederalPECOCode() {
		return federalPECOCode;
	}

	public void setFederalPECOCode(String federalPECOCode) {
		this.federalPECOCode = federalPECOCode;
	}

	public String getStatePECOCode() {
		return statePECOCode;
	}

	public void setStatePECOCode(String statePECOCode) {
		this.statePECOCode = statePECOCode;
	}

	public String getContributionPECOCode() {
		return contributionPECOCode;
	}

	public void setContributionPECOCode(String contributionPECOCode) {
		this.contributionPECOCode = contributionPECOCode;
	}

	public String getSourceCodes() {
		return sourceCodes;
	}

	public void setSourceCodes(String sourceCodes) {
		this.sourceCodes = sourceCodes;
	}

	public String getQualifiedRothDistribution() {
		return qualifiedRothDistribution;
	}

	public void setQualifiedRothDistribution(String qualifiedRothDistribution) {
		this.qualifiedRothDistribution = qualifiedRothDistribution;
	}

	public String getStateTaxWithHoldingMinimumRequirements() {
		return stateTaxWithHoldingMinimumRequirements;
	}

	public void setStateTaxWithHoldingMinimumRequirements(
			String stateTaxWithHoldingMinimumRequirements) {
		this.stateTaxWithHoldingMinimumRequirements = stateTaxWithHoldingMinimumRequirements;
	}

	public String getStateGeneralLedger() {
		return stateGeneralLedger;
	}

	public void setStateGeneralLedger(String stateGeneralLedger) {
		this.stateGeneralLedger = stateGeneralLedger;
	}

	public String getFederalGeneralLedger() {
		return federalGeneralLedger;
	}

	public void setFederalGeneralLedger(String federalGeneralLedger) {
		this.federalGeneralLedger = federalGeneralLedger;
	}

	public double getAllowedIncrements() {
		return allowedIncrements;
	}

	public void setAllowedIncrements(double allowedIncrements) {
		this.allowedIncrements = allowedIncrements;
	}

	public String getJournalSecurityEligibility() {
		return journalSecurityEligibility;
	}

	public void setJournalSecurityEligibility(String journalSecurityEligibility) {
		this.journalSecurityEligibility = journalSecurityEligibility;
	}

	public String getW4PFormIGO() {
		return w4PFormIGO;
	}

	public void setW4PFormIGO(String w4pFormIGO) {
		w4PFormIGO = w4pFormIGO;
	}

	public double getStateTaxWitholdingPercentage() {
		return stateTaxWitholdingPercentage;
	}

	public void setStateTaxWitholdingPercentage(
			double stateTaxWitholdingPercentage) {
		this.stateTaxWitholdingPercentage = stateTaxWitholdingPercentage;
	}

	public double getActualTransactionAmount() {
		return actualTransactionAmount;
	}

	public void setActualTransactionAmount(double actualTransactionAmount) {
		this.actualTransactionAmount = actualTransactionAmount;
	}

	public String getManagedAccountEligiblity() {
		return managedAccountEligiblity;
	}

	public void setManagedAccountEligiblity(String managedAccountEligiblity) {
		this.managedAccountEligiblity = managedAccountEligiblity;
	}

	public String getCash() {
		return Cash;
	}

	public void setCash(String cash) {
		Cash = cash;
	}

	public List<DOCADocuments> getDocumentCodes() {
		return documentCodes;
	}

	public void setDocumentCodes(List<DOCADocuments> documentCodes) {
		this.documentCodes = documentCodes;
	}

	public @BusinessType("com.lpl.virtualdomains.RetailOrRetirement") String getDeliveringAccountRetailOrRetirement() {
		return deliveringAccountRetailOrRetirement;
	}

	public void setDeliveringAccountRetailOrRetirement(
			String deliveringAccountRetailOrRetirement) {
		this.deliveringAccountRetailOrRetirement = deliveringAccountRetailOrRetirement;
	}

	public @BusinessType("com.lpl.virtualdomains.RetailOrRetirement") String getReceivingAccountRetailOrRetirement() {
		return receivingAccountRetailOrRetirement;
	}

	public void setReceivingAccountRetailOrRetirement(
			String receivingAccountRetailOrRetirement) {
		this.receivingAccountRetailOrRetirement = receivingAccountRetailOrRetirement;
	}

}
